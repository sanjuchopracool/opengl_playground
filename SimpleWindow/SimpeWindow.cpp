#include "SimpeWindow.h"
#include <iostream>

SimpeWindow::SimpeWindow(QWidget *parent)
    : QOpenGLWidget(parent)
{
}

SimpeWindow::~SimpeWindow()
{
    glDeleteProgram(mProgram);
}
static void print_shader_log(GLuint shader)
{
    std::string str;
    GLint len;

    glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &len);
    if (len != 0)
    {
        str.resize(len);
        glGetShaderInfoLog(shader, len, NULL, &str[0]);
    }

    std::cout << "Shader :" << str << std::endl;
}

void SimpeWindow::initializeGL()
{
    glewInit();
    // vertex shader
    GLuint vertex_shader;
    GLuint fragment_shader;

    static const GLchar* vertex_shader_source[] =
    {
        "#version 410 core                                          \n"
        "                                                           \n"
        "const vec4 vertices[3] = vec4[3](vec4(0.0, 0.5, 0.5, 1.0),  \n"
        "                                 vec4(-0.5, -0.5, 0.5, 1.0), \n"
        "                                 vec4(0.5, -0.5, 0.5, 1.0));  \n"
        "void main(void)                                            \n"
        "{                                                          \n"
        "   gl_Position = vertices[gl_VertexID];                    \n"
        "}                                                          \n"
    };

    static const GLchar* fragment_shader_source[] =
    {
        "#version 410 core                                          \n"
        "                                                           \n"
        "out vec4 color;                                            \n"
        "                                                           \n"
        "void main(void)                                            \n"
        "{                                                          \n"
        "   color = vec4(0.0, 0.8, 1.0, 1.0 );                      \n"
        "}                                                          \n"
    };
    vertex_shader = glCreateShader( GL_VERTEX_SHADER );
    glShaderSource(vertex_shader, 1, vertex_shader_source, NULL);
    glCompileShader(vertex_shader);
    print_shader_log(vertex_shader);

    fragment_shader = glCreateShader( GL_FRAGMENT_SHADER );
    glShaderSource(fragment_shader, 1, fragment_shader_source, NULL);
    glCompileShader(fragment_shader);
    print_shader_log(fragment_shader);

    mProgram = glCreateProgram();
    glAttachShader(mProgram, vertex_shader);
    glAttachShader(mProgram, fragment_shader);
    glLinkProgram(mProgram);

    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
}

void SimpeWindow::paintGL()
{
    static const GLfloat green[] = { 0.0f, 0.25f, 0.0f, 1.0f };
    glClearBufferfv(GL_COLOR, 0, green);

    glUseProgram(mProgram);
    glDrawArrays(GL_TRIANGLES, 0, 3);
}

void SimpeWindow::resizeGL(int width, int height)
{
    glViewport( 0, 0, width, height );
}
