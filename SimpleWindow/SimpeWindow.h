#ifndef SIMPEWINDOW_H
#define SIMPEWINDOW_H

#include <GL/glew.h>
#include <QOpenGLWidget>

class SimpeWindow : public QOpenGLWidget
{
    Q_OBJECT

public:
    SimpeWindow(QWidget *parent = 0);
    ~SimpeWindow();

protected:
    void initializeGL() override;
    void paintGL() override;
    void resizeGL(int width, int height) override;

private:
    GLuint mProgram;
};

#endif // SIMPEWINDOW_H
