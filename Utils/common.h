#ifndef COMMON_H
#define COMMON_H

#include <iostream>

enum class ExceptionCode
{
    FAILED_TO_OPEN_FILE = 0,
    ERROR_IN_SHADER_COMPILATION
};

class OpenGLException
{
    public:
    OpenGLException( ExceptionCode inCode, const std::string& inExtraInfo = std::string() )
        : mCode( inCode ), mExtraInfo( inExtraInfo )
    {
    }

    virtual std::string Message() const {
        return mExtraInfo;
    }

private:
    ExceptionCode mCode;
    std::string mExtraInfo;
};
#endif // COMMON_H
