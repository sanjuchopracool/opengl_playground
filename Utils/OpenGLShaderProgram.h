#ifndef OPENGLSHADERPROGRAM_H
#define OPENGLSHADERPROGRAM_H

#include "OpenGLObject.h"
#include "OpenGLShader.h"

class OpenGLShaderProgram : public OpenGLObject
{
public:
    OpenGLShaderProgram();
    ~OpenGLShaderProgram();

    void attachShader( const OpenGLShader& inShader);
    void attachShader( OpenGLShader::ShaderType inType,  const std::string &inFilePath = std::string() );
    void link();
    void bind();
};

#endif // OPENGLSHADERPROGRAM_H
