
INCLUDEPATH += \
            "."

HEADERS += \
    OpenGLObject.h \
    OpenGLShader.h \
    OpenGLShaderProgram.h

SOURCES += \
    OpenGLObject.cpp \
    OpenGLShader.cpp \
    OpenGLShaderProgram.cpp
