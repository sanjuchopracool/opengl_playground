#-------------------------------------------------
#
# Project created by QtCreator 2018-01-27T13:30:59
#
#-------------------------------------------------

QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG-=app_bundle

TARGET = FrameWorkTester
TEMPLATE = app

INCLUDEPATH += /usr/local/Cellar/glew/2.1.0/include \
               /usr/local/Cellar/boost/1.66.0/include

LIBS += -L/usr/local/Cellar/glew/2.1.0/lib -lGLEW

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
        main.cpp \
        TesterWindow.cpp \
    ../Utils/OpenGLObject.cpp \
    ../Utils/OpenGLShader.cpp \
    ../Utils/OpenGLShaderProgram.cpp

HEADERS += \
        TesterWindow.h \
    ../Utils/OpenGLObject.h \
    ../Utils/OpenGLShader.h \
    ../Utils/OpenGLShaderProgram.h \
    ../Utils/common.h

DISTFILES += \
    ../Shaders/Triangle/triangle.vert \
    ../Shaders/Triangle/triangle.frag

