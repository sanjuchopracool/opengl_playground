#include "TesterWindow.h"
#include "../Utils/OpenGLShader.h"

#include <math.h>
#include <QDateTime>
#include <QTimer>

TesterWindow::TesterWindow(QWidget *parent)
    : QOpenGLWidget(parent)
{
    QTimer* timer = new QTimer(this);
    connect( timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(20);
}

TesterWindow::~TesterWindow()
{

}

void TesterWindow::initializeGL()
{
    glewInit();

    // create program
    mProgram.reset( new OpenGLShaderProgram );
    mProgram->attachShader( OpenGLShader::ShaderType::Vertex, "../Shaders/Triangle/triangle.vert" );
    mProgram->attachShader( OpenGLShader::ShaderType::Fragment, "../Shaders/Triangle/triangle.frag" );
    mProgram->link();

    GLuint vao;
    glGenVertexArrays(1, &vao);
    glBindVertexArray(vao);
}

void TesterWindow::resizeGL(int w, int h)
{
    glViewport(0, 0, w, h);
}

void TesterWindow::paintGL()
{
    static const GLfloat green[] = { 0.0f, 0.25f, 0.0f, 1.0f };
    glClearBufferfv(GL_COLOR, 0, green);

    // The time used by sb7(aka book is in the following pattern)
    double currentTime = QDateTime::currentMSecsSinceEpoch()/1000.0;
    GLfloat attrib[] = { (float)sin(currentTime) ,
                         (float)cos(currentTime) ,
                         0.0f, 0.0f };

    mProgram->bind();

    // update the value of input attribute 0
    glVertexAttrib4fv(0 , attrib);

    glDrawArrays(GL_TRIANGLES, 0, 3);
}
