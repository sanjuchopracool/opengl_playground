#include "TesterWindow.h"
#include <QApplication>

#include "../Utils/common.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QSurfaceFormat format(QSurfaceFormat::DeprecatedFunctions);
    format.setMajorVersion(4);
    format.setMinorVersion(5);
    format.setProfile( QSurfaceFormat::CoreProfile );
    format.setDepthBufferSize(24);
    format.setRenderableType( QSurfaceFormat::RenderableType::OpenGL );
    QSurfaceFormat::setDefaultFormat(format);

    try
    {
        TesterWindow w;
        w.show();
        return a.exec();
    }
    catch ( OpenGLException ex )
    {
        std::cout << ex.Message() << std::endl;
    }
}
