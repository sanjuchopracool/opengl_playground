#ifndef TESTERWINDOW_H
#define TESTERWINDOW_H

#include <GL/glew.h>
#include <QOpenGLWidget>
#include "../Utils/OpenGLShaderProgram.h"
#include <memory>

class TesterWindow : public QOpenGLWidget
{
    Q_OBJECT

public:
    TesterWindow(QWidget *parent = 0);
    ~TesterWindow();

protected:
    virtual void initializeGL() override;
    virtual void resizeGL(int w, int h) override;
    virtual void paintGL() override;

private:
    std::unique_ptr<OpenGLShaderProgram> mProgram;
    GLuint mVAO;
};

#endif // TESTERWINDOW_H
